package io2016.Tests;
import io2016.DataAccessLayer;
import javafx.collections.ObservableList;

import java.util.ArrayList;

/**
 * Created by Kuba on 2017-01-04.
 */
public class DataAccessLayerTest {

    public boolean convertPreferetHoursTest(ArrayList<ObservableList<Integer>> hoursPreferences,ArrayList<ArrayList<Integer>> preferedHours) {

        for(int i=0;i<5;i++){
            for(int j=0;j<hoursPreferences.get(i).size();j++){
                int valH=hoursPreferences.get(i).get(j).intValue();
                int valP=preferedHours.get(i).get(valH).intValue()-1;
                if(valH!=valP) return false;
            }
        }
        return true;
    }
}
