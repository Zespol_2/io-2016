package io2016.Tests;

import io2016.DataAccessLayer;
import io2016.InternalDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Kuba on 2017-01-19.
 */
public class TestAll {

    InternalDB internalDB;
    private Connection conn;
    DataAccessLayer dataAccessLayer;
    public static boolean validate =false;

    public static void main(String[] args) throws IOException, SQLException {
        DataAccessLayer dt=new DataAccessLayer();
        TestAll t=new TestAll(dt);
    }

    public TestAll(DataAccessLayer dataAccessLayer) throws IOException, SQLException {
        this.dataAccessLayer=dataAccessLayer;
        validate =true;
        runTests();

    }

    public void generateTestStudent(int id) throws SQLException, IOException {



        ArrayList<ObservableList<Integer>> prefferedHours=generateRandomHours();

        dataAccessLayer.removeAllUserPreferences(6000,true);
        System.out.print("\nTest wprowadzania danych dla studenta nr."+(id+1)+" ");
        dataAccessLayer.savePreferencesToDB(prefferedHours,6000);
        dataAccessLayer.testJsonStudent();
        dataAccessLayer.removeAllUserPreferences(6000,true);



    }



    public void generateTestLecturer(int id) throws SQLException, IOException {

       // generateLecturerTest();

        ArrayList<ObservableList<Integer>> preferedHours=generateRandomHours();
        ObservableList<Integer> preferedRooms=generateRandomRooms();

        dataAccessLayer.removeAllUserPreferences(8000,false);
        System.out.print("\nTest wprowadzania danych dla wykładowcy nr."+(id+1)+" ");
        dataAccessLayer.savePreferencesToDB(preferedHours,preferedRooms,8000);
        dataAccessLayer.testJsonLecturer();
        dataAccessLayer.removeAllUserPreferences(8000,false);



    }

    public ArrayList<ObservableList<Integer>> generateRandomHours(){
        Random rand=new Random();
        int[] sizes=new int[5];
        ArrayList<ObservableList<Integer>> prefferedHours=new ArrayList<>();
        for(int i=0;i<5;i++) {
            sizes[i]=rand.nextInt(13);
            ObservableList<Integer> list= FXCollections.observableArrayList();

            //prefferedHours.add(list)
            for(int j=0;j<sizes[i];j++){
                //ArrayList<Integer> numbers=new ArrayList<>();
                int number;
                boolean success=false;
                do{
                    number=rand.nextInt(13);
                    success=search(list,number);
                } while (success!=true);
                list.add(number);
            }
            Collections.sort(list);
            prefferedHours.add(list);
        }
        return prefferedHours;
    }

    public ObservableList<Integer> generateRandomRooms(){
        Random rand=new Random();

        ObservableList<Integer> preferedRooms=FXCollections.observableArrayList();

            int size=rand.nextInt(19);

            for(int j=0;j<size;j++){

                int number;
                boolean success=false;
                do{
                    number=rand.nextInt(19)+2;
                    success=search(preferedRooms,number);
                } while (success!=true);
                preferedRooms.add(number);
            }
            Collections.sort(preferedRooms);

       // }
        return preferedRooms;
    }

    private boolean search(ObservableList<Integer> list,int number){
        for(int k=0;k<list.size();k++){
            if(list.get(k)==number){
                return false;
            }
        } return true;
    }

    public void testStudentSeries() throws SQLException, IOException {

        generateUserTest();

        for(int i=0;i<20;i++){
            generateTestStudent(i);
        }

        removeUserTest();

    }

    public void testLecturerSeries() throws SQLException, IOException {

        generateLecturerTest();

        for(int i=0;i<20;i++){
            generateTestLecturer(i);
        }

        removeLecturerTest();

    }




    public void runTests() throws SQLException, IOException {
        this.dataAccessLayer=dataAccessLayer;
        conn=dataAccessLayer.getInternalDB().getConnection();
        internalDB=dataAccessLayer.getInternalDB();
        //generateTestStudent();
        //generateTestLecturer();

        InternalDBTest t=new InternalDBTest(internalDB,conn);

        System.out.println("TESTOWANIE POLĄCZENIA");
        t.testConnection();

        System.out.println("TESTY WPROWADZANIA DANYCH: student");
        testStudentSeries();

        System.out.println("TESTY WPROWADZANIA DANYCH: wykladowca");
        testLecturerSeries();


        System.out.println("\nTESTOWANIE POLĄCZENIA");
        t.testConnection();

        int poprawne=InternalDBTest.poprawne+JsonOutputTest.poprawne;
        int niepoprawne=InternalDBTest.niepoprawne+JsonOutputTest.niepoprawne;
        System.out.println("\n" +
                "\nPODSUMOWANIE");
        System.out.println("\nLiczba testow zakończonych powodzeniem: "+poprawne);
        System.out.println("Liczba testow zakonczonych niepowodzeniem: "+niepoprawne);

    }

    private void generateUserTest() throws SQLException {

            PreparedStatement test=conn.prepareStatement("SELECT * FROM `students` WHERE `id`=6000");
            ResultSet rs=test.executeQuery();
            if(!rs.next()) {

                PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO " +
                        "`students` (`id`, `firstname`, `lastname`,`group_id`) VALUES (?,?,?,?)");
                preparedStatement.setInt(1, 6000);
                preparedStatement.setString(2, "test");
                preparedStatement.setString(3, "testowy");
                preparedStatement.setInt(4, 6);
                preparedStatement.executeUpdate();
            }
    }

    private void generateLecturerTest() throws SQLException {

        PreparedStatement test=conn.prepareStatement("SELECT * FROM `lecturers` WHERE `id`=8000");
        ResultSet rs=test.executeQuery();
        if(!rs.next()) {
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO " +
                    "`lecturers` (`id`, `firstname`, `lastname`) VALUES (?,?,?)");
            preparedStatement.setInt(1, 8000);
            preparedStatement.setString(2, "testL");
            preparedStatement.setString(3, "testowyL");
            preparedStatement.executeUpdate();
        }
    }

    public void removeUserTest() throws SQLException {
        PreparedStatement test=conn.prepareStatement("SELECT * FROM `students` WHERE `id`=6000");
        ResultSet rs=test.executeQuery();
        if(!rs.next()) {
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM `students` " +
                    "WHERE `id`=(?)");
            preparedStatement.setInt(1, 6000);
            preparedStatement.executeUpdate();
        }
    }

    public void removeLecturerTest() throws SQLException {
        PreparedStatement test=conn.prepareStatement("SELECT * FROM `lecturers` WHERE `id`=8000");
        ResultSet rs=test.executeQuery();
        if(!rs.next()) {
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM `lecturers` " +
                    "WHERE `id`=(?)");
            preparedStatement.setInt(1, 8000);
            preparedStatement.executeUpdate();
        }
    }
}
