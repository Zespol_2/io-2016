package io2016.Tests;

import io2016.GroupPreferences;
import io2016.LecturerPreferences;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Kuba on 2017-01-13.
 */
public class JsonOutputTest {

    public static int poprawne=0;
    public static int niepoprawne=0;

    public void testConversion(ArrayList<ObservableList<Integer>> hoursPreferences,ArrayList<ArrayList<Integer>> preferedHours){
        DataAccessLayerTest testDAL=new DataAccessLayerTest();
        boolean validation_test=testDAL.convertPreferetHoursTest(hoursPreferences,preferedHours);
        if(!validation_test) {
            System.out.println("\n-Test konwersji list zakończony niepowodzeniem");
            niepoprawne++;
        }
        else {
            System.out.println("\n-Test konwersji list zakończony pomyślnie");
            poprawne++;
        }
    }

    public void testJSonStudent(ArrayList<GroupPreferences> groupsPreferences) throws FileNotFoundException {
        boolean validation_test=testAggregatedStudentsPreferrations(groupsPreferences);
        if(!validation_test) {
            System.out.println("-Test tworzenia JSon dla ucznia list zakończony niepowodzeniem");
            niepoprawne++;
        }
        else {
            System.out.println("-Test tworzenia JSon dla ucznia zakończony pomyślnie");
            poprawne++;
        }
    }


    public void testJSonLecturer(ArrayList<LecturerPreferences> lecturersPreferences) throws IOException {
        boolean validation_test=testAggregatedLecturerPreferrations(lecturersPreferences);
        if(!validation_test) {
            System.out.println("-Test tworzenia JSon dla wykladowcy list zakończony niepowodzeniem");
            niepoprawne++;
        }
        else {
            System.out.println("-Test tworzenia JSon dla wykladowcy zakończony pomyślnie");
            poprawne++;
        }
    }

    public boolean testAggregatedStudentsPreferrations(ArrayList<GroupPreferences> groupsPreferences) throws FileNotFoundException {


        ArrayList<GroupPreferences> preferences=new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("output/groupsPreferredHours.json"));
        try {

            String line = br.readLine();
            JSONArray mArray = new JSONArray(line);
            int id_count = 0;

            for (int i = 0; i < mArray.length(); i++) {
                try {
                    int group_id = mArray.getJSONObject(i).getInt("group_id");
                    int day_id = mArray.getJSONObject(i).getInt("day_id");
                    int hour_id = mArray.getJSONObject(i).getInt("hour_id");
                    int count = mArray.getJSONObject(i).getInt("count");

                    GroupPreferences record=null;
                    for(GroupPreferences g:preferences){
                        if(g.getGroupId()==group_id) {
                            record = g;
                            Pair pair = new Pair(day_id, hour_id);
                            record.getPreferredHoursInSpecifiedDay().put(pair, count);

                        }
                    }
                    if(record==null) {
                        record = new GroupPreferences(group_id);
                        Pair pair = new Pair(day_id, hour_id);
                        HashMap<Pair<Integer, Integer>, Integer> map = new HashMap<>();
                        map.put(pair, count);
                        record.setPreferredHoursInSpecifiedDay(map);
                        preferences.add(record);

                    }

                    id_count = id_count + 1;
                } catch (JSONException e) {

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        for(int i=0;i<groupsPreferences.size();i++){
            GroupPreferences g=groupsPreferences.get(i);
            GroupPreferences p=preferences.get(i);
            Iterator itG = g.getPreferredHoursInSpecifiedDay().entrySet().iterator();
            while (itG.hasNext()) {
                Map.Entry<Pair<Integer,Integer>,Integer> pairG = (Map.Entry)itG.next();
                if(p.getPreferredHoursInSpecifiedDay().get(pairG.getKey())==null)
                {
                    System.out.println("blad");
                    return false;
                }
            }
        }

        return true;
    }



    public boolean testAggregatedLecturerPreferrations(ArrayList<LecturerPreferences> lecturersPreferences) throws IOException {


        ArrayList<LecturerPreferences> preferences=new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("output/lecturersPreferredHours.json"));
        BufferedReader br2 = new BufferedReader(new FileReader("output/roomPreferences.json"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            JSONArray mArray = new JSONArray(line);

            StringBuilder sb2 = new StringBuilder();
            String line2 = br2.readLine();
            JSONArray mArrayRooms = new JSONArray(line2);
            int id_count = 0;
            int devices_count = 0;
            //int tmp = 0;
            ArrayList<Pair<Integer,Integer>> rooms=new ArrayList<>();
            for (int i = 0; i < mArrayRooms.length(); i++) {
                int lecturer_id = mArrayRooms.getJSONObject(i).getInt("lecturer_id");
                int room = mArrayRooms.getJSONObject(i).getInt("room_id");
                rooms.add(new Pair<>(lecturer_id,room));
            }

            for (int i = 0; i < mArray.length(); i++) {
                try {
                    int lecturer_id = mArray.getJSONObject(i).getInt("lecturer_id");
                    int day_id = mArray.getJSONObject(i).getInt("day_id");
                    int hour_id = mArray.getJSONObject(i).getInt("hour_id");

                    LecturerPreferences record=null;
                    for(LecturerPreferences l:preferences) {
                        if (l.getLecturerId() == lecturer_id) {
                            record = l;
                            Pair pair = new Pair(day_id, hour_id);
                            record.getPreferredHoursInSpecifiedDay().put(pair, 1);

                        }
                    }
                    if(record==null){
                        record = new LecturerPreferences(lecturer_id);
                        Pair pair = new Pair(day_id, hour_id);
                        HashMap<Pair<Integer, Integer>, Integer> mapa = new HashMap<>();
                        mapa.put(pair, 1);

                        record.setPreferredHoursInSpecifiedDay(mapa);
                        ArrayList<Integer> pokoje=new ArrayList<>();
                        for(Pair<Integer,Integer> p:rooms){
                            if(p.getKey()==lecturer_id){
                                pokoje.add(p.getValue());
                            }
                        }
                        record.setRoomList(pokoje);
                        preferences.add(record);

                    }

                    id_count = id_count + 1;
                } catch (JSONException e) {
                    // If id doesn't exist, this exception is thrown
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        for(int i=0;i<lecturersPreferences.size();i++){
            LecturerPreferences g=lecturersPreferences.get(i);
            LecturerPreferences p=preferences.get(i);
            Iterator itG = g.getPreferredHoursInSpecifiedDay().entrySet().iterator();
            while (itG.hasNext()) {
                Map.Entry<Pair<Integer,Integer>,Integer> pairG = (Map.Entry)itG.next();
                if(p.getPreferredHoursInSpecifiedDay().get(pairG.getKey())==null)
                {
                    System.out.println("blad");
                    return false;
                }
                for(int j=0;j<g.getRoomList().size();j++){
                    int a=g.getRoomList().get(j);
                    int b=p.getRoomList().get(j);
                    if(a!=b) {
                        System.out.println("blad, pokoje");
                        return false;
                    }

                }
            }
        }

        return true;
    }
}
