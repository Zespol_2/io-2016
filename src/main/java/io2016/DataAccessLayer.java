package io2016;

import io2016.Tests.JsonOutputTest;
import io2016.Tests.TestAll;
import javafx.collections.ObservableList;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ishfi on 14.12.2016.
 *
 * Bridge to mysql database and to json format support
 */
public class DataAccessLayer {
    private InternalDB internalDB;
    private JsonOutput jsonOutput;

    /**
     * Gets object which handles database operations
     * @return Object which handles database operations
     */
    public InternalDB getInternalDB() {
        return internalDB;
    }

    /**
     * Initialization of connection to database and writer to json output files
     */
    public DataAccessLayer() {
        this.internalDB = new InternalDB();
        this.jsonOutput = new JsonOutput();
    }

    /**
     * Save lecturer preferences to database
     * @param hoursPreferences list of preferred hours
     * @param roomsIdsPreferences list of preferred rooms
     * @param userId current logged lecturer
     * @throws SQLException
     */
    public void savePreferencesToDB(ArrayList<ObservableList<Integer>> hoursPreferences,
                                    ObservableList<Integer> roomsIdsPreferences, int userId) throws SQLException {
        ArrayList<ArrayList<Integer>> preferredHours = convertPreferetHours(hoursPreferences);

        internalDB.addLecturersPreferences(preferredHours, roomsIdsPreferences, userId);
    }

    /**
     * Saves studetn preferences to database
     * @param hoursPreferences list of preferred hours
     * @param userId current logged Student
     * @throws SQLException
     */
    public void savePreferencesToDB(ArrayList<ObservableList<Integer>> hoursPreferences, int userId) throws SQLException {
        ArrayList<ArrayList<Integer>> preferredHours = convertPreferetHours(hoursPreferences);

        internalDB.addStudentsPreferences(preferredHours, userId);
    }

    /**
     * Verifies user credentials
     * @param index login
     * @param lastname password
     * @param student true -> student, false -> lecturer
     * @return Pair of user id and status of verification
     * @throws SQLException
     */
    public Pair<Integer, Boolean> verifyLoginCredentials(String index, String lastname, Boolean student) throws SQLException {
        Pair<Integer, Boolean> verifiedCredentials;

        verifiedCredentials = internalDB.checkUserCredentials(index,lastname,student);

        return verifiedCredentials;
    }

    /**
     * Gets list of available rooms from database
     * @return list of rooms ids and their names
     * @throws SQLException
     */
    public Pair<ArrayList<Integer>,ObservableList<String>> obtainRoomsFromDB() throws SQLException {
        Pair<ArrayList<Integer>,ObservableList<String>> roomIdsAndNames;

        roomIdsAndNames = internalDB.getRoomList();

        return roomIdsAndNames;
    }

    /**
     * Removes all user preferrences from database
     * @param userId current logged user
     * @param student true -> student, false -> lecturer
     * @throws SQLException
     */
    public void removeAllUserPreferences(int userId, Boolean student) throws SQLException {
        if (student){
            internalDB.removeAllStudentPreferencesRecords(userId);
        }else{
            internalDB.removeAllLecturerPreferencesRecords(userId);
            internalDB.removeAllLecturerRoomsPreferencesRecords(userId);
        }
    }

    /**
     * Gets preferred hours from database
     * @param userId current logged student
     * @param student true -> student, false -> lecturer
     * @return list of preferred hours
     * @throws SQLException
     */
    public ArrayList<Pair<Integer,Integer>> getPreferedHoursFromDB(int userId, Boolean student) throws SQLException {
        return internalDB.getUserPreferredHours(userId, student);
    }

    /**
     * Gets preferred rooms from database
     * @param lecturerId current logged lecturer
     * @return list of preferred rooms
     * @throws SQLException
     */
    public ArrayList<Integer> getPreferedRoomsFromDB(int lecturerId) throws SQLException {
        return internalDB.getLecturerPreferredRooms(lecturerId);
    }

    public void testJsonStudent() throws IOException, SQLException {
        ArrayList<GroupPreferences> groupsPreferences = new ArrayList<GroupPreferences>();




        groupsPreferences = internalDB.selectAggregatedStudents();
        jsonOutput.saveAggregatedStudentsPreferrations(groupsPreferences);

        /* test */

        if(TestAll.validate) {
            JsonOutputTest testy = new JsonOutputTest();
            testy.testJSonStudent(groupsPreferences);
        }

        /* test */
    }

    public void testJsonLecturer() throws IOException, SQLException {

        ArrayList<LecturerPreferences> lecturersPreferences = new ArrayList<LecturerPreferences>();
        lecturersPreferences = internalDB.selectAggregatedLecturer();
        jsonOutput.saveAggregatedLecturerPreferrences(lecturersPreferences);

        internalDB.selectAggregatedRooms(lecturersPreferences);
        jsonOutput.saveAggregatedRoomPreferrences(lecturersPreferences);



        /* test */

        if(TestAll.validate) {
            JsonOutputTest testy=new JsonOutputTest();
            testy.testJSonLecturer(lecturersPreferences);
        }


        /* test */
    }

    /**
     * Saves preferences from database to json files
     * @throws IOException
     * @throws SQLException
     */
    public void saveToJsonFile() throws IOException, SQLException {
        ArrayList<LecturerPreferences> lecturersPreferences = new ArrayList<LecturerPreferences>();
        ArrayList<GroupPreferences> groupsPreferences = new ArrayList<GroupPreferences>();


        JsonOutputTest testy;

        groupsPreferences = internalDB.selectAggregatedStudents();
        jsonOutput.saveAggregatedStudentsPreferrations(groupsPreferences);

        /* test */
        if(TestAll.validate){
            testy=new JsonOutputTest();
            testy.testJSonStudent(groupsPreferences);
        }


        /* test */

        lecturersPreferences = internalDB.selectAggregatedLecturer();
        jsonOutput.saveAggregatedLecturerPreferrences(lecturersPreferences);

        internalDB.selectAggregatedRooms(lecturersPreferences);
        jsonOutput.saveAggregatedRoomPreferrences(lecturersPreferences);

        /* test */

        if(TestAll.validate) {
            testy=new JsonOutputTest();
            testy.testJSonLecturer(lecturersPreferences);
        }

        /* test */
    }

    /**
     * Converts ArrayList of ObservableList to ArrayList of ArrayList
     * @param hoursPreferences list of preferred hours (ObservableList)
     * @return list of preferred hours (ArrayList)
     */
    //TODO: Why?
    private ArrayList<ArrayList<Integer>> convertPreferetHours(ArrayList<ObservableList<Integer>> hoursPreferences){
        ArrayList<ArrayList<Integer>> preferedHours = new ArrayList<>(5);

        for (int i = 0; i < 5; ++i){
            int index = 0;
            preferedHours.add(new ArrayList<Integer>());
            for (int j = 0; j < 13; ++j){
                int size=hoursPreferences.get(i).size();
                if(index<size){
                    if (hoursPreferences.get(i).get(index) == j){
                        preferedHours.get(i).add(j+1);
                        index++;
                    }else{
                        preferedHours.get(i).add(0);
                    }
                } else{
                    preferedHours.get(i).add(0);
                }

            }
        }

        /* test */
        if(TestAll.validate) {
            JsonOutputTest testy = new JsonOutputTest();
            testy.testConversion(hoursPreferences, preferedHours);
        }
        /* test */


        return preferedHours;
    }
}
