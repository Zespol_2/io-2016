package io2016;

import io2016.Controllers.LoginController;
import io2016.Tests.InternalDBTest;
import io2016.Tests.TestAll;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by ishfi on 13.12.2016.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starting JavaFX thread
     * @param primaryStage
     * @throws IOException
     * @throws SQLException while db problems
     */
    @Override
    public void start(Stage primaryStage) throws IOException, SQLException {
        Supervisor supervisor = new Supervisor();

        /* test???? */

        Connection conn=supervisor.getDataAccessLayer().getInternalDB().getConnection();
        if(conn==null) return;

        InternalDBTest test= new InternalDBTest(supervisor.getDataAccessLayer().getInternalDB(),conn);
        //test.testConnection();

        //TestAll t=new TestAll(supervisor.getDataAccessLayer());   //testy poczatkowe, wylacza walidacje w trakcie dzialania

        //TestAll.validate =true; //uruchamia walidacje w trakcie dzialania programu
        /* test???? */

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/loginWindow.fxml"));
        Parent root = loader.load();
        LoginController controller = loader.getController();
        controller.setSupervisor(supervisor);

        primaryStage.setTitle("IO 2016");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }
}
