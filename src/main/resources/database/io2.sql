-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Sty 2017, 21:37
-- Wersja serwera: 10.1.19-MariaDB
-- Wersja PHP: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `io2`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `groups`
--

INSERT INTO `groups` (`id`, `group_name`) VALUES
(1, 'IS04'),
(2, 'IS05'),
(3, 'IS02'),
(4, 'IS01'),
(5, 'IS03'),
(6, 'IS06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lecturers`
--

CREATE TABLE `lecturers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL,
  `lastname` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `lecturers`
--

INSERT INTO `lecturers` (`id`, `firstname`, `lastname`) VALUES
(1, 'Monika', 'Pernach'),
(2, 'Lukasz', 'Rauch'),
(3, 'Kazimierz', 'Michalik'),
(4, 'Krzysztof', 'Banas'),
(5, 'Justyna', 'Odrobina'),
(6, 'Piotr', 'Kustra'),
(7, 'Andriy', 'Milenin'),
(8, 'Grzegorz', 'Gorecki'),
(9, 'Jan', 'Kusiak'),
(10, 'Jan', 'Bielanski'),
(11, 'Krzysztof', 'Banas');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `preferred_hours_lecturers`
--

CREATE TABLE `preferred_hours_lecturers` (
  `id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `hour_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `preferred_hours_lecturers`
--

INSERT INTO `preferred_hours_lecturers` (`id`, `lecturer_id`, `day_id`, `hour_id`) VALUES
(11, 8, 1, 3),
(12, 8, 1, 8),
(13, 8, 1, 11),
(14, 8, 2, 5),
(15, 8, 2, 10),
(16, 8, 3, 5),
(17, 8, 3, 10),
(18, 8, 4, 1),
(19, 8, 4, 13),
(20, 8, 5, 6),
(21, 5, 1, 4),
(22, 5, 1, 9),
(23, 5, 2, 8),
(24, 5, 3, 3),
(25, 5, 3, 11),
(26, 5, 4, 6),
(27, 5, 5, 1),
(28, 5, 5, 8),
(29, 5, 5, 12),
(30, 2, 1, 2),
(31, 2, 1, 7),
(32, 2, 1, 12),
(33, 2, 1, 13),
(34, 2, 2, 1),
(35, 2, 2, 5),
(36, 2, 2, 8),
(37, 2, 2, 11),
(38, 2, 3, 4),
(39, 2, 3, 7),
(40, 2, 3, 9),
(41, 2, 3, 13),
(42, 2, 4, 3),
(43, 2, 4, 4),
(44, 2, 4, 7),
(45, 2, 4, 10),
(46, 2, 5, 5),
(47, 2, 5, 8),
(48, 2, 5, 12),
(49, 10, 1, 2),
(50, 10, 1, 6),
(51, 10, 1, 10),
(52, 10, 2, 1),
(53, 10, 2, 5),
(54, 10, 2, 11),
(55, 10, 3, 4),
(56, 10, 3, 11),
(57, 10, 4, 4),
(58, 10, 4, 6),
(59, 10, 4, 12),
(60, 10, 5, 4),
(61, 10, 5, 12);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `preferred_hours_students`
--

CREATE TABLE `preferred_hours_students` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `hour_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `preferred_hours_students`
--

INSERT INTO `preferred_hours_students` (`id`, `user_id`, `day_id`, `hour_id`) VALUES
(1, 2017, 1, 5),
(2, 2017, 1, 9),
(3, 2017, 1, 12),
(4, 2017, 2, 8),
(5, 2017, 3, 5),
(6, 2017, 3, 10),
(7, 2017, 4, 4),
(8, 2017, 4, 8),
(9, 2017, 5, 7),
(10, 2017, 5, 10),
(11, 2017, 5, 13),
(12, 2500, 1, 2),
(13, 2500, 1, 3),
(14, 2500, 1, 4),
(15, 2500, 1, 6),
(16, 2500, 1, 7),
(17, 2500, 1, 8),
(18, 2500, 1, 11),
(19, 2500, 2, 2),
(20, 2500, 2, 3),
(21, 2500, 2, 6),
(22, 2500, 2, 8),
(23, 2500, 2, 10),
(24, 2500, 2, 11),
(25, 2500, 3, 5),
(26, 2500, 3, 9),
(27, 2500, 4, 7),
(28, 2500, 4, 10),
(29, 2500, 5, 3),
(30, 2500, 5, 5),
(31, 2500, 5, 9),
(32, 2411, 1, 5),
(33, 2411, 1, 7),
(34, 2411, 1, 10),
(35, 2411, 2, 5),
(36, 2411, 2, 9),
(37, 2411, 2, 10),
(38, 2411, 3, 6),
(39, 2411, 4, 4),
(40, 2411, 4, 6),
(41, 2411, 4, 8),
(42, 2411, 4, 9),
(43, 2411, 4, 10),
(44, 2411, 4, 12),
(45, 2411, 5, 5),
(46, 2411, 5, 9),
(47, 2411, 5, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `preferred_rooms`
--

CREATE TABLE `preferred_rooms` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `preferred_rooms`
--

INSERT INTO `preferred_rooms` (`id`, `room_id`, `lecturer_id`) VALUES
(4, 3, 8),
(5, 11, 8),
(6, 15, 8),
(7, 3, 5),
(8, 11, 5),
(9, 15, 5),
(10, 7, 5),
(11, 9, 5),
(12, 12, 5),
(13, 2, 2),
(14, 3, 2),
(15, 6, 2),
(16, 13, 2),
(17, 15, 2),
(18, 20, 2),
(19, 2, 10),
(20, 3, 10),
(21, 6, 10),
(22, 13, 10),
(23, 15, 10),
(24, 20, 10),
(25, 10, 10),
(26, 15, 10),
(27, 18, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_nr` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `building` varchar(5) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `rooms`
--

INSERT INTO `rooms` (`id`, `room_nr`, `type_id`, `building`) VALUES
(2, 109, 1, 'B5'),
(3, 110, 3, 'B5'),
(4, 101, 1, 'B5'),
(5, 102, 1, 'B5'),
(6, 201, 2, 'B5'),
(7, 608, 1, 'B5'),
(8, 603, 2, 'B5'),
(9, 1, 4, 'SWFiS'),
(10, 108, 1, 'B4'),
(11, 301, 2, 'D10'),
(12, 104, 3, 'B4'),
(13, 2, 4, 'SWFiS'),
(14, 302, 2, 'B4'),
(15, 303, 1, 'B4'),
(16, 305, 1, 'B5'),
(17, 306, 1, 'B4'),
(18, 208, 3, 'B4'),
(19, 10, 3, 'A2'),
(20, 3, 3, 'U2');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rooms_type`
--

CREATE TABLE `rooms_type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `rooms_type`
--

INSERT INTO `rooms_type` (`id`, `type`) VALUES
(1, 'Laboratoryjna'),
(2, 'Audytoryjna'),
(3, 'Wykładowa'),
(4, 'Sportowa');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `firstname` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL,
  `lastname` varchar(32) COLLATE utf8mb4_polish_ci NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `students`
--

INSERT INTO `students` (`id`, `firstname`, `lastname`, `group_id`) VALUES
(1978, 'Pawel', 'Mikoljow', 6),
(2017, 'Donald', 'Trup', 5),
(2111, 'Michal', 'Korman', 3),
(2112, 'Krzysztof', 'Kobiela', 3),
(2113, 'Rafal', 'Krawczuk', 3),
(2114, 'Konrad', 'Klimczak', 3),
(2115, 'Adam', 'Klimko', 4),
(2116, 'Michal', 'Kaczmarski', 6),
(2118, 'John', 'Rambo', 4),
(2137, 'Karol', 'Wojtyla', 4),
(2211, 'Mateusz', 'Suwara', 1),
(2212, 'Szymon', 'Durak', 4),
(2411, 'Patryk', 'Jakubiec', 5),
(2500, 'Jan', 'Nowak', 2),
(2501, 'Adam', 'Malysz', 6),
(2732, 'Robert', 'Kubica', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `subject`
--

INSERT INTO `subject` (`id`, `name`, `status_id`) VALUES
(18, 'Inzynieria internetu', 3),
(19, 'Inzynieria internetu W', 4),
(20, 'Inzynieria oprogramowania', 3),
(21, 'Inzynieria oprogramowania W', 4),
(22, 'Jezyk angielski', 3),
(23, 'Metoda elementow skonczonych', 2),
(24, 'Metoda elementów skonczonych W', 4),
(25, 'Podstawy sztucznej inteligencji	', 2),
(26, 'Podstawy sztucznej inteligencji W', 4),
(27, 'Programowanie równolegle', 2),
(28, 'Programowanie równolegle W', 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `subject_lecturer`
--

CREATE TABLE `subject_lecturer` (
  `id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `subject_lecturer`
--

INSERT INTO `subject_lecturer` (`id`, `lecturer_id`, `subject_id`) VALUES
(19, 1, 18),
(20, 2, 19),
(21, 3, 20),
(22, 4, 21),
(23, 5, 22),
(24, 6, 23),
(25, 7, 24),
(26, 8, 25),
(27, 8, 26),
(28, 9, 26),
(29, 11, 28),
(30, 10, 27);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `subject_status`
--

CREATE TABLE `subject_status` (
  `id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `subject_status`
--

INSERT INTO `subject_status` (`id`, `status`) VALUES
(1, 'Uniwersalny'),
(2, 'Laboratoryjny'),
(3, 'Audytoryjny'),
(4, 'Wykładowy'),
(5, 'Sportowy');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preferred_hours_lecturers`
--
ALTER TABLE `preferred_hours_lecturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preferred_hours_students`
--
ALTER TABLE `preferred_hours_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preferred_rooms`
--
ALTER TABLE `preferred_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_type` (`type_id`);

--
-- Indexes for table `rooms_type`
--
ALTER TABLE `rooms_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_gr` (`group_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_status` (`status_id`);

--
-- Indexes for table `subject_lecturer`
--
ALTER TABLE `subject_lecturer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub` (`subject_id`),
  ADD KEY `fk_lectur` (`lecturer_id`);

--
-- Indexes for table `subject_status`
--
ALTER TABLE `subject_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `preferred_hours_lecturers`
--
ALTER TABLE `preferred_hours_lecturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT dla tabeli `preferred_hours_students`
--
ALTER TABLE `preferred_hours_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT dla tabeli `preferred_rooms`
--
ALTER TABLE `preferred_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT dla tabeli `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT dla tabeli `rooms_type`
--
ALTER TABLE `rooms_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT dla tabeli `subject_lecturer`
--
ALTER TABLE `subject_lecturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT dla tabeli `subject_status`
--
ALTER TABLE `subject_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`type_id`) REFERENCES `rooms_type` (`id`);

--
-- Ograniczenia dla tabeli `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `fk_gr` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Ograniczenia dla tabeli `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `fk_status` FOREIGN KEY (`status_id`) REFERENCES `subject_status` (`id`);

--
-- Ograniczenia dla tabeli `subject_lecturer`
--
ALTER TABLE `subject_lecturer`
  ADD CONSTRAINT `fk_lectur` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturers` (`id`),
  ADD CONSTRAINT `fk_sub` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
