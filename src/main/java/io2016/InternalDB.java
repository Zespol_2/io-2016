package io2016;

import io2016.Tests.InternalDBTest;
import io2016.Tests.TestAll;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ishfi on 14.12.2016.
 *
 * Handles  in/out operation to database
 */
public class InternalDB {

    public Connection getConnection() {
        return connection;
    }

    private Connection connection;

    /**
     * Setting connection to database
     */
    public InternalDB() {
        connection=connect();
    }

    /**
     * Connecting to database
     * @return
     */
    private Connection connect(){
        Connection conn=null;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/io2?user=user&password=user");
            connection=conn;
            return conn;
        } catch (SQLException ex) {
            //handle errors
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Uwaga!");
            alert.setHeaderText("Brak połączenia z bazą!");
            alert.setContentText("Prosze sprawdzić połączenie z serwerem!");
            alert.showAndWait();


            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return null;
    }

    /**
     * Adds student preferences to database
     * @param preferredHours list of preferred hours
     * @param studentId current logged student
     * @throws SQLException
     */
    public void addStudentsPreferences(ArrayList<ArrayList<Integer>> preferredHours, int studentId) throws SQLException {
        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j){
                if (preferredHours.get(i).get(j) != 0){
                    addStudentPreferences(studentId, i+1, preferredHours.get(i).get(j));
                }
            }
        }

        /* test */

        if(TestAll.validate) {

            InternalDBTest test = new InternalDBTest(this, connection);
            test.studentHoursTest(studentId, preferredHours);

        }
        /* test */
    }

    /**
     * Adds lecturer preferences to database
     * @param preferredHours list of preferred hours
     * @param roomsIdsPreferences list of preferred rooms
     * @param lecturerId current logged lecturer
     * @throws SQLException
     */
    public void addLecturersPreferences(ArrayList<ArrayList<Integer>> preferredHours,
                                       ObservableList<Integer> roomsIdsPreferences, int lecturerId) throws SQLException {
        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j){
                if (preferredHours.get(i).get(j) != 0){
                    addLecturerPreferences(lecturerId, i+1, preferredHours.get(i).get(j));
                }
            }
        }

        for (Integer roomId: roomsIdsPreferences) {
            addRoomPreferences(lecturerId, roomId);
        }



        /* test */

        //

        if(TestAll.validate) {

            InternalDBTest test=new InternalDBTest(this,connection);
            test.testConnection();
            test.lecturerHoursTest(lecturerId, preferredHours);
            test.roomTest(lecturerId, roomsIdsPreferences);

        }
        /* test */
    }

    private void addStudentPreferences(int studentId, int dayId, int hourId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
                "`preferred_hours_students` (`user_id`, `day_id`, `hour_id`) VALUES (?,?,?)");
        preparedStatement.setInt(1,studentId);
        preparedStatement.setInt(2,dayId);
        preparedStatement.setInt(3,hourId);
        preparedStatement.executeUpdate();
    }

    private void addLecturerPreferences(int lecturerId, int dayId, int hourId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
                "`preferred_hours_lecturers` (`lecturer_id`, `day_id`, `hour_id`) VALUES (?,?,?)");
        preparedStatement.setInt(1,lecturerId);
        preparedStatement.setInt(2,dayId);
        preparedStatement.setInt(3,hourId);
        preparedStatement.executeUpdate();
    }

    private void addRoomPreferences(int lecturerId, int roomId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
                "`preferred_rooms`(`room_id`, `lecturer_id`) VALUES (?,?)");
        preparedStatement.setInt(1,roomId);
        preparedStatement.setInt(2,lecturerId);
        preparedStatement.executeUpdate();
    }

    /**
     * Removes all previous student preferences
     * @param studentId current logged student
     * @throws SQLException
     */
    public void removeAllStudentPreferencesRecords(int studentId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `preferred_hours_students` " +
                "WHERE `user_id`=(?)");
        preparedStatement.setInt(1,studentId);
        preparedStatement.executeUpdate();
    }

    /**
     * Removes lecturer's preferred hours
     * @param lecturerId current logged lecturer
     * @throws SQLException
     */
    public void removeAllLecturerRoomsPreferencesRecords(int lecturerId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `preferred_rooms` " +
                "WHERE `lecturer_id`=(?)");
        preparedStatement.setInt(1,lecturerId);
        preparedStatement.executeUpdate();
    }

    /**
     * Removes lecturer's preferred rooms
     * @param lecturerId current logged lecturer
     * @throws SQLException
     */
    public void removeAllLecturerPreferencesRecords(int lecturerId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `preferred_hours_lecturers` " +
                "WHERE `lecturer_id`=(?)");
        preparedStatement.setInt(1,lecturerId);
        preparedStatement.executeUpdate();
    }

    /**
     * Gets user's preferred hours from database
     * @param userId current logged user
     * @param student true -> student, false -> lecturer
     * @return list of preferred hours
     * @throws SQLException
     */
    public ArrayList<Pair<Integer,Integer>> getUserPreferredHours(int userId, Boolean student) throws SQLException {
        ArrayList<Pair<Integer,Integer>> preferredHours = new ArrayList<>();
        PreparedStatement preparedStatement;

        if (student){
            preparedStatement = connection.prepareStatement("SELECT `day_id`, `hour_id` " +
                    "FROM `preferred_hours_students` WHERE `user_id`=(?)");
            preparedStatement.setInt(1, userId);
        }else{
            preparedStatement = connection.prepareStatement("SELECT `day_id`, `hour_id` " +
                    "FROM `preferred_hours_lecturers` WHERE `lecturer_id`=(?)");
            preparedStatement.setInt(1, userId);
        }

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                preferredHours.add(new Pair<>(resultSet.getInt(1), resultSet.getInt(2)));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return  preferredHours;
    }

    /**
     * Gets lecturer's preferred rooms
     * @param lecturerId current logged lecturer
     * @return list of preferred rooms
     * @throws SQLException
     */
    public ArrayList<Integer> getLecturerPreferredRooms(int lecturerId) throws SQLException {
        ArrayList<Integer> preferredRooms = new ArrayList<>();

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT `room_id` FROM " +
                "`preferred_rooms` WHERE `lecturer_id`=(?)");
        preparedStatement.setInt(1, lecturerId);

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                preferredRooms.add(resultSet.getInt(1));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return  preferredRooms;
    }

    /**
     * Checks user credentials
     * @param index login
     * @param lastname password
     * @param student true -> student, false -> lecturer
     * @return Pair of user id and status of verification
     * @throws SQLException
     */
    public Pair<Integer,Boolean> checkUserCredentials(String index, String lastname, Boolean student) throws SQLException {
        Boolean success = false;
        int userId = 0;
        Statement statement = connection.createStatement();

        if (student){
            try (ResultSet resultSet = statement.executeQuery("SELECT `id`, `lastname` FROM `students`")) {
                while (resultSet.next()) {
                    userId = resultSet.getInt(1);
                    if ((Integer.parseInt(index) == userId)
                            && (lastname.equals(resultSet.getString(2)))){
                       success = true;
                       break;
                    }
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
        }else{
            try (ResultSet resultSet = statement.executeQuery("SELECT `id`, `lastname` FROM `lecturers`")) {
                while (resultSet.next()) {
                    userId = resultSet.getInt(1);
                    if ((Integer.parseInt(index) == userId)
                            && (lastname.equals(resultSet.getString(2)))){
                        success = true;
                        break;
                    }
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
        }
        return new Pair<Integer,Boolean>(userId,success);
    }

    /**
     * Gets all available rooms from database
     * @return list of room ids and their names
     * @throws SQLException
     */
    public Pair<ArrayList<Integer>,ObservableList<String>> getRoomList() throws SQLException {
        Pair<ArrayList<Integer>,ObservableList<String>> roomIdsAndNames;
        List<String> list = new ArrayList<String>();
        ObservableList<String> roomList = FXCollections.observableList(list);
        ArrayList<Integer> roomIdList = new ArrayList<>();
        Statement statement = connection.createStatement();

        try (ResultSet resultSet = statement.executeQuery("SELECT `id`, `room_nr`, `building` FROM `rooms`")) {
            while (resultSet.next()) {
                roomIdList.add(resultSet.getInt(1));
                roomList.add(String.valueOf(resultSet.getInt(2)) + " " + resultSet.getString(3));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        roomIdsAndNames = new Pair<>(roomIdList,roomList);

        return roomIdsAndNames;
    }

    //--------------------------------For Json------------------------------

    /**
     * Gets groups preferences form database
     * @return list of group's preferences
     * @throws SQLException
     * @throws IOException
     */
    public  ArrayList<GroupPreferences> selectAggregatedStudents() throws SQLException, IOException {
        Statement statement = connection.createStatement();
        ArrayList<GroupPreferences> groupsPreferences = new ArrayList<>();

        try (ResultSet resultSet = statement.executeQuery(
                "SELECT group_id, day_id, hour_id, count(user_id) as count " +
                        "FROM students JOIN preferred_hours_students " +
                        "ON students.id = preferred_hours_students.user_id " +
                        "GROUP BY group_id, day_id, hour_id")) {

            while (resultSet.next()) {
                int groupIndex = -1;

                if (groupsPreferences.size() == 0){
                    groupsPreferences.add(new GroupPreferences(resultSet.getInt(1)));
                    groupIndex = groupsPreferences.size() - 1;
                }else{
                    for (int i = 0; i < groupsPreferences.size(); ++i){
                        if (groupsPreferences.get(i).getGroupId() == resultSet.getInt(1)){
                            groupIndex = i;
                        }
                    }
                    if (groupIndex == -1){
                        groupsPreferences.add(new GroupPreferences(resultSet.getInt(1)));
                        groupIndex = groupsPreferences.size() - 1;
                    }
                }

                groupsPreferences.get(groupIndex).getPreferredHoursInSpecifiedDay().put(new Pair<Integer,Integer>
                                (resultSet.getInt(2), resultSet.getInt(3))
                            , resultSet.getInt(4));
            }
        }
        return groupsPreferences;
    }

    /**
     * Gets lecturer's hours preferences form database
     * @return lecturers hours preferences
     * @throws SQLException
     * @throws IOException
     */
    public ArrayList<LecturerPreferences> selectAggregatedLecturer() throws SQLException, IOException {
        Statement statement = connection.createStatement();
        ArrayList<LecturerPreferences> lecturersPreferences = new ArrayList<>();

        try (ResultSet resultSet = statement.executeQuery(
                "SELECT lecturer_id, day_id, hour_id, count(lecturer_id) as count FROM lecturers " +
                        "JOIN preferred_hours_lecturers ON lecturers.id = preferred_hours_lecturers.lecturer_id " +
                        "GROUP BY lecturer_id, day_id, hour_id")){
            while (resultSet.next()) {
                int groupIndex = -1;

                if (lecturersPreferences.size() == 0){
                    lecturersPreferences.add(new LecturerPreferences(resultSet.getInt(1)));
                    groupIndex = lecturersPreferences.size() - 1;
                }else{
                    for (int i = 0; i < lecturersPreferences.size(); ++i){
                        if (lecturersPreferences.get(i).getLecturerId() == resultSet.getInt(1)){
                            groupIndex = i;
                        }
                    }
                    if (groupIndex == -1){
                        lecturersPreferences.add(new LecturerPreferences(resultSet.getInt(1)));
                        groupIndex = lecturersPreferences.size() - 1;
                    }
                }

                lecturersPreferences.get(groupIndex).getPreferredHoursInSpecifiedDay().put(new Pair<Integer,Integer>
                                (resultSet.getInt(2), resultSet.getInt(3))
                        , resultSet.getInt(4));
            }
        }
        return lecturersPreferences;
    }

    /**
     * Gets lecturer's rooms preferences form database
     * @param lecturersPreferences list of lecturers with theirs preferences
     * @throws SQLException
     * @throws IOException
     */
    public void selectAggregatedRooms(ArrayList<LecturerPreferences> lecturersPreferences) throws SQLException, IOException {
        Statement statement = connection.createStatement();

        try (ResultSet resultSet = statement.executeQuery(
                "SELECT `lecturer_id`, `room_id` FROM `preferred_rooms` ORDER BY `lecturer_id` ASC")) {
            while (resultSet.next()) {
                int index = -1;
                for (int i = 0; i < lecturersPreferences.size(); ++i){
                    if (lecturersPreferences.get(i).getLecturerId() == resultSet.getInt(1)){
                        index = i;
                        break;
                    }
                }

                if (index == -1){
                    lecturersPreferences.add(new LecturerPreferences(resultSet.getInt(1)));
                    index = lecturersPreferences.size() - 1;
                }

                lecturersPreferences.get(index).getRoomList().add(resultSet.getInt(2));
            }
        }
    }

}