package io2016.Tests;

import io2016.InternalDB;
import io2016.Supervisor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Kuba on 2017-01-13.
 */
public class InternalDBTest {

    public Connection connection;
    private InternalDB internalDB;

    public static int poprawne=0;
    public static int niepoprawne=0;

    public InternalDBTest(InternalDB internalDB, Connection conn){
        this.internalDB=internalDB;
        this.connection=conn;
    }

    public void testConnection() throws SQLException {
        if(connection.isValid(50)) {
            System.out.println("-Test połączenia z bazą zakończony pomyślnie");
            poprawne++;
        }
        else {
            System.out.println("-Test połączenia z bazą zakończony niepowodzeniem");
            niepoprawne++;
        }
    }

    public void studentHoursTest(Integer studentId, ArrayList<ArrayList<Integer>> preferredHours) throws SQLException {
        boolean testHour=true;

        ArrayList<ArrayList<Integer>> test1=getStudentPreferedHours(studentId);
        int val1=0;
        int val2=0;
        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j) {
                if(preferredHours.get(i).get(j)!=0) val1++;
                if(test1.get(i).get(j)!=0) val2++;
            }
        }

        if(val1!=val2) {
            System.out.println("-Test ilości godzin zakończony niepowodzeniem");
            niepoprawne++;
        } else {
            System.out.println("-Test ilości godzin zakończony pomyślnie");
            poprawne++;
        }

        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j) {
                if(preferredHours.get(i).get(j)!=test1.get(i).get(j)) testHour=false;
            }
        }

        if(!testHour) {
            System.out.println("-Test wprowadzenia danych godzin studenta do bazy zakończony niepowodzeniem");
            niepoprawne++;
        }

        else {
            System.out.println("-Test wprowadzenia danych godzin studenta do bazy zakończony pomyślnie");
            poprawne++;
        }
    }

    //private void testHoursCount()

    public void lecturerHoursTest(Integer lecturerId, ArrayList<ArrayList<Integer>> preferredHours) throws SQLException {
        boolean testHour=true;


        ArrayList<ArrayList<Integer>> test1=getLecturerPreferedHours(lecturerId);
        int val1=0;
        int val2=0;
        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j) {
                if(preferredHours.get(i).get(j)!=0) val1++;
                if(test1.get(i).get(j)!=0) val2++;
            }
        }

        if(val1!=val2) {
            System.out.println("-Test ilości godzin zakończony niepowodzeniem");
            niepoprawne++;
        } else {
            System.out.println("-Test ilości godzin zakończony pomyślnie");
            poprawne++;
        }


        for (int i = 0; i < preferredHours.size(); ++i){
            for (int j = 0; j < preferredHours.get(i).size(); ++j) {
                if(preferredHours.get(i).get(j)!=test1.get(i).get(j)) testHour=false;
            }
        }


        if(!testHour) {
            System.out.println("-Test wprowadzenia danych godzin wykladowcy do bazy zakończony niepowodzeniem");
            niepoprawne++;
        }
        else{
            System.out.println("-Test wprowadzenia danych godzin wykladowcy do bazy zakończony pomyślnie");
            poprawne++;
        }
    }

    public ObservableList<String> getRoomsValues(ObservableList<Integer> preferedRoomsIds){

        ObservableList<String> preferedRoomsValues= FXCollections.observableArrayList();
        for(int i=0;i<preferedRoomsIds.size();i++)
        {
            int id=preferedRoomsIds.get(i);
            preferedRoomsValues.add(Supervisor.roomIdsAndNames.getValue().get(id-2));
        }
        return preferedRoomsValues;
    }

    public void roomTest(Integer lecturerId, ObservableList<Integer> roomsPreferences) throws SQLException {
        boolean testRoom=true;
        ArrayList<Integer> test2=this.internalDB.getLecturerPreferredRooms(lecturerId);
        if(test2.size()!=roomsPreferences.size()){
            System.out.println("-Test ilości pokoi zakończony niepowodzeniem");
            niepoprawne++;
        } else {
            System.out.println("-Test ilości pokoi zakończony pomyślnie");
            poprawne++;
        }

        for (int i = 0; i < roomsPreferences.size(); ++i){
            {
                if(!(roomsPreferences.get(i)==(test2.get(i)))) testRoom=false;
            }
        }

        if(!testRoom){
            System.out.println("-Test wprowadzenia danych pokoi do bazy zakończony niepowodzeniem");
            niepoprawne++;
        }
        else {
            System.out.println("-Test wprowadzenia danych pokoi do bazy zakończony pomyślnie");
            poprawne++;
        }
    }

    //-------------------------------For Tests-----------------------------------

    public ArrayList<ArrayList<Integer>> getLecturerPreferedHours(Integer lecturerId) throws SQLException {

        ArrayList<ArrayList<Integer>> preferedHours=new ArrayList<>(5);
        for(int i=0;i<5;i++) {
            preferedHours.add(new ArrayList<>());
            for(int j=0;j<13;j++)
            {
                preferedHours.get(i).add(0);
            }
        }


        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM preferred_hours_lecturers WHERE lecturer_id=(?)");
        preparedStatement.setInt(1,lecturerId);
        ResultSet resultSet=preparedStatement.executeQuery();

        while(resultSet.next()){
            int day=resultSet.getInt("day_id");
            day-=1;
            int hour=resultSet.getInt("hour_id");

            preferedHours.get(day).set(hour-1,hour);
        }

        return preferedHours;
    }



    public ArrayList<ArrayList<Integer>> getStudentPreferedHours(Integer studentId) throws SQLException {

        ArrayList<ArrayList<Integer>> preferedHours=new ArrayList<>(5);
        for(int i=0;i<5;i++) {
            preferedHours.add(new ArrayList<>());
            for(int j=0;j<13;j++)
            {
                preferedHours.get(i).add(0);
            }
        }


        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM preferred_hours_students WHERE user_id=(?)");
        preparedStatement.setInt(1,studentId);
        ResultSet resultSet=preparedStatement.executeQuery();

        while(resultSet.next()){
            int day=resultSet.getInt("day_id");
            day-=1;
            int hour=resultSet.getInt("hour_id");

            preferedHours.get(day).set(hour-1,hour);
        }

        return preferedHours;
    }


    /*public ArrayList<Integer> getLecturerPreferredRooms(int lecturerId) throws SQLException {
        ArrayList<Integer> preferredRooms = new ArrayList<>();

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT `room_id` FROM " +
                "`preferred_rooms` WHERE `lecturer_id`=(?)");
        preparedStatement.setInt(1, lecturerId);

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                preferredRooms.add(resultSet.getInt(1));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return  preferredRooms;
    }*/

    //--------------------------------------------------------------------------------------------------------

}
