package io2016;

import io2016.Tests.TestAll;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Created by ishfi on 14.12.2016.
 *
 * Facade to mysql database and json
 */
public class Supervisor {
    private int userId;
    private boolean student;

    private DataAccessLayer dataAccessLayer = new DataAccessLayer();
    public static Pair<ArrayList<Integer>,ObservableList<String>> roomIdsAndNames;
    private ArrayList<ObservableList<Integer>> hoursPreferences;
    private ObservableList<Integer> preferedRoomsIds = FXCollections.observableArrayList();


    /**
     *
     * @return Object of dataAccessLayer
     */
    public DataAccessLayer getDataAccessLayer() {
        return dataAccessLayer;
    }

    /**
     * Checks if student
     * @return true -> student, false -> lecturer
     */
    public boolean isStudent() {
        return student;
    }

    /**
     * Set student parameter
     * @param student true or false
     */
    public void setStudent(boolean student) {
        this.student = student;
    }

    /**
     * Set list of preferred hours
     * @param hoursPreferences list of preferred hours
     */
    public void setHoursPreferences(ArrayList<ObservableList<Integer>> hoursPreferences) {
        this.hoursPreferences = hoursPreferences;
    }

    /**
     * Set list of preferred rooms
     * @param roomsPreferences list of preferred rooms
     */
    public void setRoomsPreferences(ObservableList<String> roomsPreferences) {
        preferedRoomsIds = FXCollections.observableArrayList();

        for (String s: roomsPreferences) {
            for (int i = 0; i < roomIdsAndNames.getValue().size(); i++){
                if (s.equals(roomIdsAndNames.getValue().get(i))){
                    preferedRoomsIds.add(roomIdsAndNames.getKey().get(i));
                }
            }
        }
    }

    /**
     * Tries to authenticate user
     * @param index login
     * @param lastname password
     * @param callback it is activated on authentication response
     * @throws SQLException
     */
    public void login(String index, String lastname,  Consumer<Boolean> callback) throws SQLException {
        Pair<Integer, Boolean> verifiedCredentials;
        verifiedCredentials = dataAccessLayer.verifyLoginCredentials(index, lastname, student);

        userId = verifiedCredentials.getKey();
        callback.accept(verifiedCredentials.getValue());
    }

    /**
     * Gets rooms form db
     * @return List of rooms
     * @throws SQLException
     */
    public Pair<ArrayList<Integer>,ObservableList<String>> getRoomsList() throws SQLException {
        roomIdsAndNames = dataAccessLayer.obtainRoomsFromDB();

        return roomIdsAndNames;
    }

    /**
     * Gets previous selected hours from db
     * @return List of hours
     * @throws SQLException
     */
    public ArrayList<Pair<Integer,Integer>> getPreviousPreferedHours() throws SQLException {
        return dataAccessLayer.getPreferedHoursFromDB(userId,student);
    }

    /**
     * Get previous selected rooms from db
     * @return List of rooms
     * @throws SQLException
     */
    public ArrayList<Integer> getPreviousPreferedRooms() throws SQLException {
        return dataAccessLayer.getPreferedRoomsFromDB(userId);
    }

    /**
     * Removes all user's preferences
     * @throws SQLException
     */
    public void removePreviousUserPreferences() throws SQLException {
        dataAccessLayer.removeAllUserPreferences(userId,student);
    }

    /**
     * Saves preferences to db and to json files
     * @throws SQLException
     * @throws IOException
     */
    public void save() throws SQLException, IOException {
        if(student){
            dataAccessLayer.savePreferencesToDB(hoursPreferences,userId);
        }else{
            dataAccessLayer.savePreferencesToDB(hoursPreferences,preferedRoomsIds,userId);
        }

        dataAccessLayer.saveToJsonFile();
    }
}
